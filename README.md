# Hello Application example


This example shows how to build and deploy a containerized Go web server
application using [Kubernetes](https://kubernetes.io).
Inspired by [sample web application](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/hello-app).


Visit https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html 
to install all of the required resources to get started with Amazon EKS using eksctl, a simple command line utility for creating and managing Kubernetes clusters on Amazon EKS.

Create application with commands:
	- `kubectl apply -f helloweb-app.yaml`
	- `kubectl apply -f helloweb-service.yaml`
	- `kubectl get svc service-helloweb -o yaml`

Deploy this application on [Amazon Elastic Kubernetes Service](https://aws.amazon.com/eks).

This repository also contains:

- `main.go` contains the HTTP server implementation. It responds to all HTTP
  requests with a  `Hello, world!` response.
- `Dockerfile` is used to build the Docker image for the application.


This application is available as Docker image on DockerHub:

- `bitbucketpipelines/hello-app-eks:11` initial version of application
